package training.web;

import javax.ejb.Stateful;
import javax.ejb.Stateless;

@Stateless
public class Counter {
	private int count;
	
	public int increment() {
		return ++count;
	}

}
