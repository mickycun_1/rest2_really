package training.web;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/")
public class SimpleController {
	@GET
	@Path("/hello")
	public String hello(){
		return "hello world";
	}
	
	@GET @Path("/greet/{name}")
	public String greet(@PathParam("name") String name){
		return String.format("hello %s", name);
	}
	
	@GET @Path("/speak")
	public String speak(@QueryParam("who") String who){
		return String.format("Greetings %s", who);
	}
	
	@POST @Path("/speak")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String speakForm(@FormParam("who") String who){
		return String.format("ciao %s", who);
	}
	// we have a different verb here but the same URL. 
	
	@EJB
	Counter counterBean;
	
	@GET @Path("/counter")
	public String counter(){
		return String.format("Visitor count: %d", counterBean.increment());
	}
	
	
}
